#!/bin/bash

#INstall binarys..
binarys=('php' 'composer' 'contao-clear' 'get_php_version' 'restart-httpd' 'start-dev' 'stop-dev');
for bin in "${binarys[@]}"
do
  curl -o /usr/local/bin/$bin https://gitlab.com/d.wyschka/Migration/raw/master/$bin
  chmod +x /usr/local/bin/$bin
done;