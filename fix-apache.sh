#!/bin/bash
if [ ! -f "/usr/local/etc/httpd/default_vhost.conf" ];
then
  USERDIRECTORY=$( echo "$(eval echo "~$(whoami)")" | sed 's/ /\\ /g' )

  wget --no-check-certificate https://gitlab.com/d.wyschka/Migration/raw/master/default_vhost.conf -P /usr/local/etc/httpd/
  sed -i.bak "s|\$USERDIRECTORY|$USERDIRECTORY|g" /usr/local/etc/httpd/default_vhost.conf
else
  echo "Default Vhost exists!";
fi;