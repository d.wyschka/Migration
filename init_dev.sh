#!/bin/bash
currentUser=$(whoami);
#Install Homebrew
which -s brew

if [[ $? != 0 ]] ; then

  echo "Install Brew..";
  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
else
  echo "Brew is already installed!";
fi;
#we need an older version of icu4c (58.2)
#brew install -f https://raw.githubusercontent.com/Homebrew/homebrew-core/e1b518a229fa4ccc76a7b6243a4bf7af29a9fe74/Formula/icu4c.rb

#Install wget --no-check-certificate
$(which brew) install wget unzip autoconf pkg-config icu4c imagemagick

#Update brew
$(which brew) update

#Disable Builtin Apache from OSX
sudo apachectl -k stop
sudo launchctl unload -w /System/Library/LaunchDaemons/org.apache.httpd.plist 2>/dev/null

#Install Apache from brew
$(which brew) install httpd

folders=("www" "www/internal" "www/internal/dokumentation" "logs" "www/internal/phpmyadmin")
#Create ProjectFolder if not exists
for i in "${folders[@]}"
do
  if [ ! -d $(eval echo "~$(whoami)")/$i ];
  then
    mkdir $(eval echo "~$(whoami)")/$i;
  fi
done
#Install phpmyadmin
wget --no-check-certificate https://files.phpmyadmin.net/phpMyAdmin/4.7.8/phpMyAdmin-4.7.8-all-languages.zip -P $(eval echo "~$(whoami)")/www/internal/phpmyadmin;
unzip $(eval echo "~$(whoami)")/www/internal/phpmyadmin/phpMyAdmin-4.7.8-all-languages.zip -d $(eval echo "~$(whoami)")/www/internal/phpmyadmin;
rm -rf $(eval echo "~$(whoami)")/www/internal/phpmyadmin/phpMyAdmin-4.7.8-all-languages.zip
cp -rf $(eval echo "~$(whoami)")/www/internal/phpmyadmin/phpMyAdmin-4.7.8-all-languages/* $(eval echo "~$(whoami)")/www/internal/phpmyadmin/
rm -rf $(eval echo "~$(whoami)")/www/internal/phpmyadmin/phpMyAdmin-4.7.8-all-languages
mv $(eval echo "~$(whoami)")/www/internal/phpmyadmin/config.sample.inc.php $(eval echo "~$(whoami)")/www/internal/phpmyadmin/config.inc.php
sed -i.bak "s|\$cfg\[\'Servers\'\]\[\$i\]\[\'AllowNoPassword\'\] = false|\$cfg\[\'Servers\'\]\[\$i\]\[\'AllowNoPassword\'\] = true|g" $(eval echo "~$(whoami)")/www/internal/phpmyadmin/config.inc.php


# Update Apache Configuration to listen on port 80 and 443
echo "Update the Apache Configuration to Listen on Default Ports";
sed -i.bak s/Listen\ 8080/Listen\ 80/g /usr/local/etc/httpd/httpd.conf
sed -i.bak s/Listen\ 8443/\#Listen\ 443/g /usr/local/etc/httpd/extra/httpd-ssl.conf

if grep -Fxq "IncludeOptional $(eval echo "~$(whoami)")/www/*/.project*.conf" /usr/local/etc/httpd/httpd.conf;
then
  echo "Config is fine";
else
  echo "IncludeOptional /usr/local/etc/httpd/default_vhost.conf" >> /usr/local/etc/httpd/httpd.conf
  echo "IncludeOptional $(eval echo "~$(whoami)")/www/*/.project*.conf" >> /usr/local/etc/httpd/httpd.conf
fi

if [ ! -f "/usr/local/etc/httpd/default_vhost.conf" ];
then
  USERDIRECTORY=$( echo "$(eval echo "~$(whoami)")" | sed 's/ /\\ /g' )

  wget --no-check-certificate https://gitlab.com/d.wyschka/Migration/raw/master/default_vhost.conf -P /usr/local/etc/httpd/
  sed -i.bak "s|\$USERDIRECTORY|$USERDIRECTORY|g" /usr/local/etc/httpd/default_vhost.conf
else
  echo "Default Vhost exists!";
fi;

#Enable Modules..
apacheModules=('mod_proxy' 'mod_macro' 'mod_watchdog' 'mod_logio' 'mod_vhost_alias' 'mod_proxy_fcgi' 'mod_deflate' 'mod_ssl' 'mod_rewrite' 'mod_socache_shmcb' 'mod_cgid')
for i in "${apacheModules[@]}"
do
  LINE=$(cat /usr/local/etc/httpd/httpd.conf | grep -n -m 1 "$i.so" | sed  's/\([0-9]*\).*/\1/')

  if [[ -z "${LINE// }" ]];
  then
    echo "$i is empty";
  else
      sed -i.bak $(eval echo "$LINE")s/\#// /usr/local/etc/httpd/httpd.conf
  fi;
done


#Install PHPVersions
phpVersions=('5.6' '7.0' '7.1' '7.2')
for i in "${phpVersions[@]}"
do
  version=${i//.}

  $(which brew) install php@$i
  $(which brew) unlink php@$i
if [ -f /usr/local/etc/php/$i/php-fpm.d/www.conf ];
then
  sed -i.bak "s|listen = 127.0.0.1:9000|listen = /tmp/php-fpm-$i.sock|g" /usr/local/etc/php/$i/php-fpm.d/www.conf
  sed -i.bak "s|user = _www|user = $currentUser|g" /usr/local/etc/php/$i/php-fpm.d/www.conf
  sed -i.bak "s|group = _www|group = staff|g" /usr/local/etc/php/$i/php-fpm.d/www.conf
  sed -i.bak "s|;listen.mode = 0660|listen.mode = 0666|g" /usr/local/etc/php/$i/php-fpm.d/www.conf

else
  sed -i.bak "s|listen = 127.0.0.1:9000|listen = /tmp/php-fpm-$i.sock|g" /usr/local/etc/php/$i/php-fpm.conf
  sed -i.bak "s|user = _www|user = $currentUser|g" /usr/local/etc/php/$i/php-fpm.conf
  sed -i.bak "s|group = _www|group = staff|g" /usr/local/etc/php/$i/php-fpm.conf
  sed -i.bak "s|;listen.mode = 0660|listen.mode = 0666|g" /usr/local/etc/php/$i/php-fpm.conf

fi
  rm -rf /private/tmp/pear/*

  PECL="/usr/local/opt/php@$i/bin/pecl"

  if [ ! -d "/usr/local/opt/php@$i" ];
  then
    PECL="/usr/local/opt/php/bin/pecl"
  fi;
  #Now the extensions
  extensions=('xdebug' 'imagick');
  for ext in "${extensions[@]}"
  do
    if [ $ext = 'xdebug' ] && [ $version = '56' ];
    then
      $PECL -C /usr/local/etc/php/$i/pear.conf install $ext-2.5.5;
    else
      $PECL -C /usr/local/etc/php/$i/pear.conf install $ext;
    fi
    $PECL uninstall -r $ext;
  done;

  #Create Install-Wrapper Script
  if [ -f "/usr/local/bin/php$version" ];
  then
    rm -rf /usr/local/bin/php$version;
  fi

  touch /usr/local/bin/php$version;
  chmod +x /usr/local/bin/php$version;
  echo "#!/bin/bash" >> /usr/local/bin/php$version;
  echo "export PHP_CURRENT=$i" >> /usr/local/bin/php$version;
  echo "/usr/local/bin/php \$@" >> /usr/local/bin/php$version;
done;

#Install Mysql
$(which brew) install mysql
#Install dnsmasq
$(which brew) install dnsmasq

#install composer
curl -o /tmp/composer-setup.php https://getcomposer.org/installer
if [ ! -d "/usr/local/opt/composer" ]; then
  mkdir /usr/local/opt/composer
fi
php /tmp/composer-setup.php --install-dir=/usr/local/opt/composer --filename=composer
rm /tmp/composer-setup.php


#Configure DNSMasq
echo 'address=/.localhost/127.0.0.1' > /usr/local/etc/dnsmasq.conf;

#add dnsmasq to resolvers
sudo mkdir -v /etc/resolver
sudo bash -c 'echo "nameserver 127.0.0.1" > /etc/resolver/localhost'

echo 'address=/.localhost/127.0.0.1' > $(brew --prefix)/etc/dnsmasq.conf
sudo cp $(brew list dnsmasq | grep /homebrew.mxcl.dnsmasq.plist$) /Library/LaunchDaemons/

sudo launchctl unload /Library/LaunchDaemons/homebrew.mxcl.dnsmasq.plist
sudo launchctl load /Library/LaunchDaemons/homebrew.mxcl.dnsmasq.plist

#INstall binarys..
binarys=('php' 'composer' 'contao-clear' 'get_php_version' 'restart-httpd' 'start-dev' 'stop-dev');
for bin in "${binarys[@]}"
do
  curl -o /usr/local/bin/$bin https://gitlab.com/d.wyschka/Migration/raw/master/$bin
  chmod +x /usr/local/bin/$bin
done;
