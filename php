#!/bin/bash
if [ "$1" == "/usr/local/bin/php" -o "$1" == "php" ]
then
	echo "Wrong syntax: $@"
	exit 1;
fi

export PHP_CURRENT=$(/usr/local/bin/get_php_version)

if [ ! -d /usr/local/opt/php\@${PHP_CURRENT} ];
then
		/usr/local/opt/php/bin/php $@
else
/usr/local/opt/php\@${PHP_CURRENT}/bin/php $@
fi
